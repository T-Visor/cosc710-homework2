/*
 * Get all comment details pertaining to each suggestion made.
 * Comments are ordered by their time stamp.
 * Suggestions are ordered by the number of upvotes (high to low).
 * Output the query results to a CSV file.
 */
SELECT sbf_suggestion.suggestionId, sbf_suggestion.votes AS suggestion_votes,  
sbf_suggestion.author AS original_poster, title, 
category, sbf_comment.commentID AS comment_ID, 
sbf_comment.author AS comment_author, sbf_comment.body AS comment_body, 
sbf_comment.timestamp AS comment_timestamp 
FROM sbf_suggestion   
INNER JOIN sbf_comment ON sbf_suggestion.suggestionId = sbf_comment.suggestionId  
ORDER BY sbf_suggestion.votes DESC
LIMIT 2000
INTO OUTFILE '/tmp/query-results.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
