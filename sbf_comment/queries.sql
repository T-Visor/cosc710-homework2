-- Display table.
DESCRIBE sbf_comment;

-- Get all authors and their comments in a thread.
SELECT author, body FROM sbf_comment 
WHERE link = '<LINK_NAME>';

-- Get all relevant information in a thread in order.
SELECT author, body, timestamp FROM sbf_comment 
WHERE link = '<LINK_NAME>' order by timestamp;

-- Order the authors by the number of posts they made.
SELECT author, COUNT(suggestionId) AS num_posts 
FROM sbf_comment 
GROUP BY author 
ORDER BY num_posts DESC;
