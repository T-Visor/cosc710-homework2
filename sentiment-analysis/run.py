#!/usr/bin/env python3

# SOURCE: https://towardsdatascience.com/the-most-favorable-pre-trained-sentiment-classifiers-in-python-9107c06442c6
import argparse
import pandas
import sys
from tqdm import tqdm
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

# GLOBALS
POSITIVE = 10
NEUTRAL = 5
NEGATIVE = 1
SENTIMENT_THRESHOLD = 0.05


def main():
    """
        Perform sentiment analysis using VADER
        (Valence Aware Dictionary and sEntiment Reasoner).
    """

    # Assign values from command-line arguments.
    arguments = parse_command_line_arguments()
    source_file = arguments.source_file[0]
    destination_file = arguments.destination_file[0]
    column_name = arguments.column_name[0]

    print('===================================')
    print('PERFORMING BATCH SENTIMENT ANALYSIS')
    print('===================================')

    sentiment_analyzer = SentimentIntensityAnalyzer()
    dataframe = pandas.read_csv(source_file)

    # Evaluate each sentence from the loaded DataFrame's identified column and overwrite it with the evaluated sentiment score.
    # Show the completion status using a progress bar.
    with tqdm(total=len(dataframe.index)) as progress_bar:
        for index, row in dataframe.iterrows():
            row_value = str(row[column_name])
            sentiment_score = get_sentiment_score(row_value, sentiment_analyzer)
            dataframe.loc[index, column_name] = sentiment_score
            progress_bar.update(1)

    # Write the modified DataFrame to a separate file (destination).
    print(str(dataframe.head(5)))
    dataframe.to_csv(destination_file)


def parse_command_line_arguments() -> argparse.ArgumentParser:
    """
        Parse the arguments from the command-line.
        If no arguments are passed, the help screen will
        be shown and the program will be terminated.

    Returns:
        (argparse.ArgumentParser): the parser with command-line arguments
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('-s', '--source_file', nargs=1, required=True,
                        help='Source CSV file.')

    parser.add_argument('-d', '--destination_file', nargs=1, required=True,
                        help='Destination filename for modified data.')

    parser.add_argument('-c', '--column_name', nargs=1, required=True,
                        help='Column of the CSV file to run sentiment analysis on.')

    # if no arguments were passed, show the help screen
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()

    return parser.parse_args()


def get_sentiment_score(sentence: str, sentiment_analyzer: SentimentIntensityAnalyzer) -> int:
    """
    Args:
        sentence (str): the sentence to analyze the sentiment of

        sentiment_analyzer (vaderSentiment.vaderSentiment.SentimentIntensityAnalyzer)
                            instance of VADER sentiment analysis tool

    Returns:
        (int): the sentiment score of the provided sentence
    """
    sentiment_dict = sentiment_analyzer.polarity_scores(sentence)

    if sentiment_dict['compound'] >= SENTIMENT_THRESHOLD:
        sentiment_score = POSITIVE
    elif sentiment_dict['compound'] <= -SENTIMENT_THRESHOLD:
        sentiment_score = NEGATIVE
    else :
        sentiment_score = NEUTRAL

    return sentiment_score


if __name__ == '__main__':
    main()
