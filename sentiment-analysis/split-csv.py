#!/usr/bin/env python3
#https://stackoverflow.com/questions/51146924/how-to-filter-out-rows-in-pandas-which-are-just-numbers-and-not-fully-numeric

import pandas

dataframe = pandas.read_csv('out/out.csv')

partial_dataframe = dataframe[['suggestionId', 'comment_ID', 'comment_body']]
partial_dataframe = partial_dataframe.set_axis(['Source', 'Target', 'Weight'], axis=1)
partial_dataframe = partial_dataframe.dropna()

print(str(partial_dataframe.tail(100)))

partial_dataframe.to_csv('out/sentiment-graph.csv', index=False)
