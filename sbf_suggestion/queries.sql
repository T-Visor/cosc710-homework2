-- Display table.
DESCRIBE sbf_suggestion;

-- Order suggestion posts by the number of votes.
SELECT author, suggestionId, votes
FROM sbf_suggestion 
GROUP BY author 
ORDER BY votes DESC;

-- Order suggestion posts by frequency.
SELECT author, count(author) AS frequency, suggestionId 
FROM sbf_suggestion
GROUP BY (author) 
ORDER BY (frequency) 
DESC limit 50;

-- Get all authors and their suggestions.
SELECT author, suggestionId 
FROM sbf_suggestion ORDER BY(author);
