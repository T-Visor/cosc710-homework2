const { initMongo } = require("./connect-mongo");
const { initSql } = require("./connect-sql");
const { Parser } = require('json2csv');
const fs = require('fs');

let mongo;
let mysql;

let commentIdsWithMentions = [];

async function main() {
    mongo = await initMongo();
    mysql = initSql();

    await buildCSVForCommentsWithMentions();
    await buildCSVForCommentsWithoutMentions();
    await buildOneCSV();

    process.exit(0);
}

async function buildOneCSV() {
    const fields = ['author', 'mention', 'body', 'commentId', 'suggestionId'];
    const opts = { fields };

    const commentsWith = await getCommentsWithMentions();
    const commentsWithout = await getCommentsWithoutMentions();
    const comments = [].concat(commentsWith, commentsWithout);

    try {
        const parser = new Parser(opts);
        const csv = parser.parse(comments);
        fs.writeFileSync('./comments.csv', csv, (e) => {
            if(e) {
                console.error(e);
            } else {
                console.log('Done!');
            }
        });
    } catch (err) {
        console.error(err);
    }
}

async function buildCSVForCommentsWithoutMentions() {
    const fields = ['author', 'mention', 'body', 'commentId', 'suggestionId'];
    const opts = { fields };

    const comments = await getCommentsWithoutMentions();

    try {
        const parser = new Parser(opts);
        const csv = parser.parse(comments);
        fs.writeFileSync('./comments_without_mentions.csv', csv, (e) => {
            if(e) {
                console.error(e);
            } else {
                console.log('Done!');
            }
        });
    } catch (err) {
        console.error(err);
    }
}

async function buildCSVForCommentsWithMentions() {
    const fields = ['author', 'mention', 'body', 'commentId', 'suggestionId'];
    const opts = { fields };

    const comments = await getCommentsWithMentions();

    try {
        const parser = new Parser(opts);
        const csv = parser.parse(comments);
        fs.writeFileSync('./comments_with_mentions.csv', csv, (e) => {
            if(e) {
                console.error(e);
            } else {
                console.log('Done!');
            }
        });
    } catch (err) {
        console.error(err);
    }
}

async function getCommentsWithoutMentions() {
    const comments = await getComments();
    const suggestions = await getSuggestions();

    let results = [];
    comments.forEach(comment => {
        const suggestion = suggestions.find(s => s.suggestionId === comment.suggestionId);

        results.push({
            author: comment.author,
            mention: suggestion.author,
            body: comment.body,
            commentId: comment.commentId,
            suggestionId: comment.suggestionId,
        });
    });
    return results;
}

async function getCommentsWithMentions() {
    const mentions = await getMentions();
    const comments = [];

    mentions.forEach(mention => {
        let usernames = extractUsername(mention.body);
        // Removes duplicate usernames for one comment
        // i.e. two @usernames in a single comment
        usernames = [... new Set(usernames)];

        if (usernames.length > 0) {
            commentIdsWithMentions.push(mention.commentId);
            usernames.forEach(username => {
                comments.push({
                    author: mention.author,
                    mention: username,
                    body: mention.body,
                    commentId: mention.commentId,
                    suggestionId: mention.suggestionId,
                })
            })
        }
    });

    return comments;
}

function extractUsername(body, pos = 0) {
    const atPos = body.indexOf('@', pos);
    let username = body.substring(atPos, body.indexOf(' ', atPos + 2));
    const regex = /@[ ]?[a-zA-Z0-9\_]*/g;

    const etc = username.split(regex)[1];

    // Clean username from extra text at end
    // i.e. '@username-' removes the '-'
    username = etc ? username.substring(1, username.indexOf(etc)) : username.substring(1);
    username = username.split(' ');
    username = username.length > 1 ? username[1] : username[0];

    // Exclude emails
    if (['gmail', 'hotmail', 'comcast', 'aol', 'yahoo'].includes(username.toLowerCase())) {
        username = '';
    }

    let recursion = [];
    if (body.indexOf('@', atPos + 2) !== -1) {
        recursion = extractUsername(body, atPos + 2);
    }

    return username ? [].concat([username], recursion) : recursion;
}

async function getSuggestions() {
    return new Promise((resolve, reject) => {
        mysql.query(`SELECT * FROM sbf_suggestion;`, (error, results, fields) => {
            if (error) reject(error);
            resolve(results);
        });
    });
}

async function getComments() {
    return new Promise((resolve, reject) => {
        mysql.query(`SELECT * FROM sbf_comment WHERE commentId NOT IN (${commentIdsWithMentions});`, (error, results, fields) => {
            if (error) reject(error);
            resolve(results);
        });
    });
}

async function getMentions() {
    return new Promise((resolve, reject) => {
        mysql.query(`SELECT * FROM sbf_comment WHERE body LIKE '%@%';`, (error, results, fields) => {
            if (error) reject(error);
            resolve(results);
        });
    });
}

main();