const { initMongo } = require("./connect-mongo");
const { initSql } = require("./connect-sql");
const { Parser } = require('json2csv');
const fs = require('fs');

let mongo;
let mysql;

async function main() {
    mongo = await initMongo();
    mysql = initSql();

    await buildCSV();

    process.exit(0);
}

async function buildCSV() {
    const fields = ['author', 'mention', 'body', 'commentId', 'suggestionId'];
    const opts = { fields };

    const comments = await getData();

    try {
        const parser = new Parser(opts);
        const csv = parser.parse(comments);
        fs.writeFileSync('./suggestion_comment.csv', csv, (e) => {
            if(e) {
                console.error(e);
            } else {
                console.log('Done!');
            }
        });
    } catch (err) {
        console.error(err);
    }
}

async function getData() {
    const suggestions = await getSuggestions();
    const comments = await getComments();

    let results = [];
    comments.forEach(comment => {
        const suggestion = suggestions.find(s => s.suggestionId === comment.suggestionId);
        results.push({
            author: comment.author,
            mention: suggestion.author,
            body: comment.body,
            commentId: comment.commentId,
            suggestionId: comment.suggestionId,
        });
    });
    return results;
}

async function getSuggestions() {
    return new Promise((resolve, reject) => {
        mysql.query(`SELECT * FROM sbf_suggestion;`, (error, results, fields) => {
            if (error) reject(error);
            resolve(results);
        });
    });
}

async function getComments() {
    return new Promise((resolve, reject) => {
        mysql.query(`SELECT * FROM sbf_comment;`, (error, results, fields) => {
            if (error) reject(error);
            resolve(results);
        });
    });
}

main();