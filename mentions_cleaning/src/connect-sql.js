const mysql = require('mysql');

/**
 * Inits and returns the SQL connection
 *
 * @returns {mysql.Connection} SQL DB connection
 */
function initSql() {
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'set_local'
    });
     
    connection.connect();

    return connection;
}
 
module.exports = { initSql };